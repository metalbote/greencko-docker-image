## Derivates ideas from https://gitlab.com/mog33/drupal8ci
## and https://hub.docker.com/u/devwithlando
FROM php:7.3-apache

ENV DBUS_SESSION_BUS_ADDRESS="/dev/null"

# Install dependencies we need
RUN mkdir -p /usr/share/man/man1 /usr/share/man/man7 \
  && apt -y update && apt-get install -y \
    curl \
    gnupg2 \
    wget

RUN  echo 'deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main' >> /etc/apt/sources.list.d/pgdg.list \
  && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

RUN echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
  && wget --quiet -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

RUN  wget --quiet -O - https://deb.nodesource.com/setup_10.x | bash -

RUN  echo 'deb https://dl.yarnpkg.com/debian/ stable main' >> /etc/apt/sources.list.d/yarn.list \
  && wget --quiet -O - https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -

RUN apt-get update \
  &&  apt-get install -y --no-install-recommends \
	  apt-transport-https \
    ca-certificates \
    exiftool \
    git \
    git-core \
    google-chrome-stable \
    imagemagick \
    jq \
    libbz2-dev \
    libc-client2007e-dev \
		libfreetype6-dev \
		libicu-dev \
		libjpeg-dev \
		libkrb5-dev \
    libldap2-dev \
		libmagickwand-dev \
		libmcrypt-dev \
    libmemcached-dev \
		libnss3-dev \
		libpng-dev \
		libpq-dev \
		libxml2-dev \
    libxslt-dev \
		libzip-dev \
		mariadb-client \
		pv \
		nodejs \
		rsync \
    software-properties-common \
    ssh \
    sudo \
    unzip \
    vim \
    wget \
    yarn \
    xfonts-base \
    xfonts-75dpi \
    zlib1g-dev;

RUN docker-php-ext-configure gd --with-freetype-dir=/usr --with-jpeg-dir=/usr --with-png-dir=/usr
RUN	docker-php-ext-configure imap --with-imap-ssl --with-kerberos
RUN docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/

RUN docker-php-ext-install -j "$(nproc)" \
      bcmath \
      bz2 \
      calendar \
      exif \
      gd \
      gettext \
      imap \
      intl \
      ldap \
      mbstring \
      mysqli \
      opcache \
      pcntl \
      pdo \
      pdo_mysql \
      pdo_pgsql \
      soap \
      sockets \
      xsl \
      zip;

RUN pecl install \
      apcu \
      imagick \
      mcrypt-1.0.2 \
      memcached \
      oauth-2.0.2 \
      redis-3.1.2;

RUN docker-php-ext-enable \
      apcu \
      imagick \
      memcached \
      mcrypt \
      oauth \
      redis;

# enable mod_rewrite
RUN if command -v a2enmod; then \
		  a2enmod rewrite; \
	  fi;

# Chromedriver
RUN curl -fsSL https://chromedriver.storage.googleapis.com/76.0.3809.68/chromedriver_linux64.zip \
  -o /tmp/chromedriver_linux64.zip \
  && unzip /tmp/chromedriver_linux64.zip -d /opt \
  && rm -f /tmp/chromedriver_linux64.zip \
  && mv /opt/chromedriver /opt/chromedriver-76.0.3809.68 \
  && chmod 755 /opt/chromedriver-76.0.3809.68 \
  && ln -fs /opt/chromedriver-76.0.3809.68 /usr/local/bin/chromedriver \
  # Add Chrome as a user
  && groupadd -r chromeuser && useradd -r -g chromeuser -G audio,video chromeuser \
  && mkdir -p /home/chromeuser && chown -R chromeuser:chromeuser /home/chromeuser

RUN	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false
RUN	apt-get clean
RUN	rm -rf /var/lib/apt/lists/* \
  && rm -rf /tmp/* \
  && rm -rf /var/tmp/*

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

# Use latest Composer
COPY --chown=www-data:www-data --from=composer:latest /usr/bin/composer /usr/local/bin/composer
COPY --chown=www-data:www-data composer.json /var/www/.composer/composer.json

RUN mkdir -p /var/www/.composer \
  && chmod 777 /var/www \
  && chown -R www-data:www-data /var/www/.composer

# Manage Composer.
WORKDIR /var/www/.composer

#install composer package to run parallel tasks
RUN composer -n global require -n "hirak/prestissimo"

# Install Drupal and PHP dev tools
RUN composer install --no-ansi -n --profile --no-suggest \
  && composer clear-cache \
  && rm -rf /var/www/.composer/cache/* \
  && rm -rf /tmp/*

WORKDIR /var/www/html

RUN chown -R www-data:www-data .

# Manage final tasks.
USER root

COPY --chown=chromeuser:chromeuser start-chromedriver.sh /scripts/start-chromedriver.sh
COPY --chown=chromeuser:chromeuser start-chrome.sh /scripts/start-chrome.sh

RUN chmod +x /scripts/*.sh \
  && ln -sf /var/www/.composer/vendor/bin/* /usr/local/bin \
  && rm -f /var/log/apache2/access.log \
  && cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini \
  && sed -i "s#memory_limit = 128M#memory_limit = 2048M#g" /usr/local/etc/php/php.ini \
  && sed -i "s#max_execution_time = 30#max_execution_time = 90#g" /usr/local/etc/php/php.ini \
  && sed -i "s#;max_input_nesting_level = 64#max_input_nesting_level = 512#g" /usr/local/etc/php/php.ini \
  && echo "alias ls='ls --color=auto -lAh'" >> /root/.bashrc \
  && echo "alias l='ls --color=auto -lAh'" >> /root/.bashrc \
  && cp /root/.bashrc /var/www/.bashrc \
  && chown www-data:www-data /var/www/.bashrc

EXPOSE 80 4444 9515 9222
